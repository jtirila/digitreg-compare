# The Great Digit Recognition Comparison

## Why is it so Empty Here?

This is something that I plan to start implementing soon. As of today (2018-10-30)
everything is in very early phases.

## Introduction

This repository is going to be the place for some notes of mine relating to a learning
project considering digit recognition using different machine learning approaches,
and a related API to offer inference services.

A partial and nonannotated list of things that may or may not find their ways into the
experiments and the resulting texts:

* TensorFlow, Keras
* SVM's
* Flask
* ...

## Related Work

The actual implementations will probably be distributed into several repositories.
I will include links here.
